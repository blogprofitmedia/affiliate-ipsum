Affiliate-Ipsum
===============

http://blogprofitmedia.com/tools/affiliate-ipsum/

Affiliate ipsum is a script written in Javascript/Jquery (and HTML/CSS) - It collects suggested keywords and creates ipsum from them.

Images and Amazon links are also added.

The output is displayed both rendered and in a textarea where you can copy the full HTML. There is no PHP/ server side script.

Included:

https://github.com/jaubourg/jquery-jsonp




