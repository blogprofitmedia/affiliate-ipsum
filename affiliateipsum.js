
// Affiliate Ipsum - Sam Woods 2014
// http://blogprofitmedia.com


;jQuery( document ).ready(function() {

	jQuery(".codelabel").hide();
	jQuery(".codetext").hide();
	//on form submit - pass the term to functionmakeTheIpsum

	jQuery('#affiliateipsum').submit(function(e) {
		e.preventDefault();

		var ipsum = '';
		var numParas = parseInt(jQuery('#numpics').val());
		var keyword = jQuery('#keyword').val().replace(/[^a-z0-9 .,?!-]/gi, "");
		var numPics = parseInt(jQuery('#numpics').val());
		var resultsSelector = '.textarea, .codetext';

		jQuery("#keyword").val(keyword);
		//If keyword is empty - show a nice warning.
		if (keyword === "") {
			jQuery("#keyword").val("Please Enter a Keyword");
			return;
		}
		//Passing all the vars to the AFFIPSUM var
		ipsum = AFFIPSUM.ipsum(keyword, numParas, numPics, resultsSelector);
		jQuery("#af_monkeypic").hide();
		jQuery(".codelabel").show();

	});

});

var AFFIPSUM = {

	keyword: "",
	numParas: 1,
	numPics: 1,
	resultsSelector: "",
	msgSelector: "",

	ipsumWords: ['elit','sed','do','eiusmod','tempor','incididunt','ut','labore','et','dolore','magna','aliqua','ut','enim','ad','minim','veniam','quis','nostrud','exercite','ullamco','laboris','nisi','ut','aliquip','ex','ea','commodo','consequat','duis','aute','irure','dolor','in','reprehenderit','in','voluptate','velit','esse','cillum','dolore','eu','fugiat','nulla','pariatur','excepteur','sint','occaecat','cupidatat','non','proident','sunt','in','culpa','qui','officia','deserunt','mollit','anim','id','est','laborum'],

	ipsum: function(keyword, numParas, numPics, resultsSelector)
	{
		AFFIPSUM.keyword = (keyword == "" ? "" : keyword);
		AFFIPSUM.numParas = (numParas == "" ? 1 : parseInt(numParas));
		AFFIPSUM.numPics = (numPics == "" ? 1 : parseInt(numPics));
		AFFIPSUM.resultsSelector = (resultsSelector == "" ? "" : resultsSelector);

		AFFIPSUM.yahoo_kws(AFFIPSUM.keyword);
	},


	yahoo_kws: function(kw)
	{

		var url = 'http://sugg.search.yahoo.com/gossip-us-fp/?nresults=10&queryfirst=2&output=yjsonp&version=&command=[KW]';
		var callback = 'yasearch';
		var query = url.replace( '[KW]', escape( kw + ' ' ) );
		var randnum = AFFIPSUM.getRandomArbitrary(1,10);

			jQuery.jsonp({

				url: query,
				callback: callback,
				cache: true,
				pageCache: true,
				timeout: 3000,

				success: function(json) {

					var results = [];
					var num_results = json.r.length;
					for( i=0; i<num_results; ++i ) {

						results[ results.length ] = json.r[i][0];
					}
					var unique = AFFIPSUM.removeDuplicates(results)
					for (var i=0; i<unique.length; ++i) {
						if (randnum>4) {
							unique[i] = '<a href="http://www.amazon.com/s/ref=nb_sb_noss/186-4747012-8094804?url=search-alias%3Daps&field-keywords=' + encodeURIComponent(unique[i]) + '">' + unique[i] + '</a>';
							randnum = AFFIPSUM.getRandomArbitrary(2,10);
						}

					}
					var extraLinks = ['<a href="http://www.amazon.com/s/ref=nb_sb_noss/186-4747012-8094804?url=search-alias%3Daps&field-keywords=' + kw + '">BUY NOW!!!' + '</a>','<a href="http://www.amazon.com/s/ref=nb_sb_noss/186-4747012-8094804?url=search-alias%3Daps&field-keywords=' + kw + '">CLICK HERE NOW</a>','<a href="http://www.amazon.com/s/ref=nb_sb_noss/186-4747012-8094804?url=search-alias%3Daps&field-keywords=' + kw + '">this is an offer you can\'t miss!</a>'];
					unique = unique.concat(extraLinks);
					AFFIPSUM.makeTheIpsum(unique);
				},

				error: function( xOptions, textStatus ) {
					console.log( 'FAIL: (' + textStatus + ')' );
				}

			});

	},

	makeTheIpsum: function(kws)
	{

		var paragraphs = [];
		var images;
		var numparas;

		if (jQuery('#numparas').val()) {
			numparas = jQuery('#numparas').val();
		}

		for (var i=0;i<numparas;i++) {
			paragraphs[paragraphs.length] = AFFIPSUM.makeAParagraph(kws);

		}

		paragraphs[0] = "Lorem ipsum dolor sit amet, " + paragraphs[0];

		for (var i=0;i<numparas;i++) {
			paragraphs[i] = "<p>" + paragraphs[i] + "</p>";
		}

		AFFIPSUM.addTheImages(paragraphs);
	},


	//Adding Images
	addTheImages: function(paragraphs)
	{

		var query = 'https://ajax.googleapis.com/ajax/services/search/images?v=1.0&callback=gImages&q=' + escape(AFFIPSUM.keyword);
		var callback = 'gImages';

		jQuery.jsonp({
			url: query,
			callback: callback,
			cache: true,
			pageCache: true,
			timeout: 3000,

			success: function(json) {

				var results = [];
				var num_results = json.responseData.results.length;
				for( i=0; i<num_results; ++i ) {
					results[ results.length ] = json.responseData.results[i]["unescapedUrl"];
				}
				var unique = AFFIPSUM.removeDuplicates(results);

				AFFIPSUM.putEverythingTogether(paragraphs, unique);
			},

			error: function( xOptions, textStatus ) {
				console.log( 'FAIL: (' + textStatus + ')' );
			}
		});
	},

	putEverythingTogether: function(paragraphs, pictures)
	{
		var numpics = AFFIPSUM.numPics;
		var images = AFFIPSUM.shuffle(pictures);
		var theIpsum = "";
		var imgCounter = 0
		var randnum = AFFIPSUM.getRandomArbitrary(1,2);

		theIpsum += "<h1>" + AFFIPSUM.giveMeAHeadline(AFFIPSUM.keyword) + "</h1>";

		theIpsum += "<p>" + paragraphs[0]; +  "</p>"
		theIpsum += '<center><a href="' + pictures[0] + '"><img src="' + pictures[0] + '" style="width:500px;" /></a></center>';
		imgCounter ++;

		for (var i=0;i<paragraphs.length-1; i++) {
			theIpsum += "<p>" + paragraphs[i+1]; + "</p>";
			if (imgCounter<numpics) {
					theIpsum += '<center><a href="' + pictures[imgCounter] + '"><img src="' + pictures[imgCounter] + '" style="width:500px;" /></a></center>';
					imgCounter++;
			}
		}
		if (imgCounter<numpics) {
			for (var i=imgCounter;i<numpics;i++) {
				theIpsum += '<center><a href="' + pictures[i] + '"><img src="' + pictures[i] + '" style="width:500px;" /></a></center>';
			}
		}
		jQuery(AFFIPSUM.resultsSelector).html(theIpsum).show();
	},

	giveMeAHeadline: function(kw)
	{
		var fixedkws = AFFIPSUM.shuffle(AFFIPSUM.ipsumWords);
		var randNum = AFFIPSUM.getRandomArbitrary(3,10);
		var headlineArray = [];
		var headline;

		for (var i=0; i<randNum;i++) {
			headlineArray[headlineArray.length] = fixedkws[i];
		}
		headlineArray[headlineArray.length] = kw;
		headline = AFFIPSUM.shuffle(headlineArray);
		return AFFIPSUM.shuffle(headline.join(" "));
	},


	makeAParagraph: function(kws)
	{
		var numsents = AFFIPSUM.getRandomArbitrary(2,9);
		var paragraph = [];
		// Get between 2 and 9 sentences for the paragraph
		for(i=0;i<numsents;i++) {
			paragraph[paragraph.length] = AFFIPSUM.giveMeASentence(kws);
		}
		// Capitalize first letter of each sentence
		for (var i=1;i<paragraph.length;i++ ) {
				paragraph[i] = paragraph[i][0].toUpperCase() + paragraph[i].slice(1);
		}
		return paragraph.join(" ");
	},


	giveMeASentence: function(kws)
	{

		var sentencedraft = [];
		var endwith, sentence;

		var mixedkw = AFFIPSUM.mixTheKeywords(kws);
		var numwords = AFFIPSUM.getRandomArbitrary(3,18)
		var endsentwith = AFFIPSUM.getRandomArbitrary(1,4);
		var comma = AFFIPSUM.getRandomArbitrary(1,3);

		//shuffle for good luck
		mixedkw = AFFIPSUM.shuffle(mixedkw);

		if (endsentwith == 4) {
			endwith = "!";
		}
		else {
			endwith = "."
		}
		for (var i=0; i< numwords; i++) {
			sentencedraft[sentencedraft.length] = mixedkw[i];
		}
		if (numwords>6 && comma == 2) {
			// make a rand num so that comma appears in the middle of sentence (min of 2 words between middle and end)
			comma = AFFIPSUM.getRandomArbitrary[2, comma.length-3];
			sentencedraft[comma] += ",";
		}
		sentence = sentencedraft.join(" ");
		sentence += endwith;
		return sentence;
	},

	mixTheKeywords: function(kws)
	{
		var mxdkws = kws.concat(AFFIPSUM.ipsumWords);
		if (kws.length < 10) {
			mxdkws = mxdkws.concat(kws);
		}

		return mxdkws;
	},


	// Returns a random number between min and max
	getRandomArbitrary: function(min, max)
	{
		return Math.random() * (max - min) + min;
	},

	//https://github.com/coolaj86/knuth-shuffle
	shuffle: function(array)
	{

		var currentIndex = array.length,
		temporaryValue,
		randomIndex;

		// While there remain elements to AFFIPSUM.shuffle...
		while (0 !== currentIndex) {
			// Pick a remaining element...
			randomIndex = Math.floor(Math.random() * currentIndex);
			currentIndex -= 1;

			// And swap it with the current element.
			temporaryValue = array[currentIndex];
			array[currentIndex] = array[randomIndex];
			array[randomIndex] = temporaryValue;
		}

		return array;
	},


	removeDuplicates: function(array)
	{
		array.sort();
		for(var i = 1; i < array.length; i++) {
			if(array[i] == array[i-1]) {
				array.splice(i--, 1);
			}
		}
		return array;
	}


}
